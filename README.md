---

## How to Install

This application is running on php 7.1 and Mysql 5.6 version. 

1. Clone the git repository 
2. change the .dev file `DATABASE_URL=mysql://root:1234@db55:3306/my_shopping` with your database configurations
3. Navigate to your project
3. Run `php bin/console doctrine:database:create`
4. Run `php bin/console doctrine:migrations:diff`
5. Run `php bin/console doctrine:fixtures:load`, This will populate sample data for product items
5. Access the project



---
### How to Make a shopping payment
1. Once you open the application you will see there the items that you can buy. 
2. Select how many items you want to buy and check the **Add to Cart**
3. Once cart is fill click on the bottom right button called **Make a Payment**
4. There will be an popup which will show what you have selected
5. Click on **Pay** button to start transaction. 
6. Click **Cancel** button to cancel the current selection. 