<?php

namespace App\DataFixtures;

use App\Entity\Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // create 20 products! Bam!

        $items = array(
          array(
              'name'=>'Kindle Paperwhite',
              'price'=>100,
              'desc'=>'Sample description for Kindle Paperwhite',
              'url'=>'https://target.scene7.com/is/image/Target/51441624_Alt01?wid=520&hei=520&fmt=pjpeg'
          ),
            array(
              'name'=>'iPad',
              'price'=>500,
                'desc'=>'Sample description for iPad',
                'url'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRgUu46zzkGYZJWzZljL2BFrybx9dmIOzmeSjuuPqVQFVVjF1bv'
          ),  array(
              'name'=>'iPhone',
              'price'=>1000,
                'desc'=>'Sample description for iPhone',
                'url'=>'https://pisces.bbystatic.com/image2/BestBuy_US/Gallery/EVN-74514_iphone_ress-18474.jpg'
          )
        ,  array(
              'name'=>'Nokia 1100',
              'price'=>10,
                'desc'=>'Sample description for Nokia 1100',
                'url'=>'http://cdn.shopify.com/s/files/1/1054/3464/products/1100_b5900710-dfa7-4b53-b5e1-b9d744bb1b5b_grande.jpg?v=1512656102'
          ),  array(
              'name'=>'Samsung J7',
              'price'=>1500,
                'desc'=>'Sample description for Samsung J7',
                'url'=>'https://i.gadgets360cdn.com/large/j5_small_1496814622843.jpg'
          ),
            array(
                'name'=>'Kindle Paperwhite 2',
                'price'=>100,
                'desc'=>'Sample description for Kindle Paperwhite',
                'url'=>'https://target.scene7.com/is/image/Target/51441624_Alt01?wid=520&hei=520&fmt=pjpeg'
            ),
            array(
                'name'=>'iPad 2',
                'price'=>500,
                'desc'=>'Sample description for iPad',
                'url'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRgUu46zzkGYZJWzZljL2BFrybx9dmIOzmeSjuuPqVQFVVjF1bv'
            ),  array(
                'name'=>'iPhone',
                'price'=>1000,
                'desc'=>'Sample description for iPhone',
                'url'=>'https://pisces.bbystatic.com/image2/BestBuy_US/Gallery/EVN-74514_iphone_ress-18474.jpg'
            )
        ,  array(
                'name'=>'Nokia 1100 2',
                'price'=>10,
                'desc'=>'Sample description for Nokia 1100',
                'url'=>'http://cdn.shopify.com/s/files/1/1054/3464/products/1100_b5900710-dfa7-4b53-b5e1-b9d744bb1b5b_grande.jpg?v=1512656102'
            ),  array(
                'name'=>'Samsung J7 2',
                'price'=>1500,
                'desc'=>'Sample description for Samsung J7',
                'url'=>'https://i.gadgets360cdn.com/large/j5_small_1496814622843.jpg'
            ),

        );
        for ($i = 0; $i < count($items); $i++) {
            $item = new Item();
            $item->setName($items[$i]['name']);
            $item->setPrice($items[$i]['price']);
            $item->setDescription($items[$i]['desc']);
            $item->setImageUrl($items[$i]['url']);
            $manager->persist($item);
        }

        $manager->flush();
    }
}
