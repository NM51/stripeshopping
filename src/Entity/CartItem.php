<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartItemRepository")
 */
class CartItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $qty;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cart", inversedBy="cartItems")
     * @ORM\JoinColumn(nullable=true)
     */
    private $cart;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Item", inversedBy="cartItem")
     * @ORM\JoinColumn(nullable=true)
     */
    private $item;

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getQty(){
        return $this->qty;
    }

    public function setQty($qty){
        $this->qty = $qty;
    }

    public function getCart(){
        return $this->cart;
    }

    public function setCart($cart){
        $this->cart = $cart;
    }

    public function getItem(){
        return $this->item;
    }

    public function setItem($item){
        $this->item = $item;
    }




}
