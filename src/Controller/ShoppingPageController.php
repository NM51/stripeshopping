<?php

namespace App\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Item;

class ShoppingPageController extends Controller
{
    /**
     * @Route("/")
     */
    public function index()
    {
        $number = mt_rand(0, 100);

        $items = $this->getDoctrine()
            ->getRepository(Item::class)
            ->findAll();

        return $this->render('shoppingPage.html.twig', array(
            'items' => $items,
        ));
    }
}